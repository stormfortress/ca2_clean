#include "Aircraft.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "ResourceHolder.hpp"
#include <iostream>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>

using namespace std::placeholders;


namespace
{
	const std::vector<AircraftData> Table = initializeAircraftData();
}

Aircraft::Aircraft(Type type, const TextureHolder& textures, const FontHolder& fonts, Direction mDir, Direction oldDir)
	: Entity(Table[type].hitpoints)
	, mType(type)
	, mSprite(textures.get(Table[type].texture), Table[type].textureRect)
	, mExplosion(textures.get(Textures::Explosion))
	, mFireCommand()
	, mMissileCommand()
	, mFireCountdown(sf::Time::Zero)
	, mIsFiring(false)
	, mIsLaunchingMissile(false)
	, mShowExplosion(true)
	, mExplosionBegan(false)
	, mSpawnedPickup(false)
	, mPickupsEnabled(true)
	, mFireRateLevel(1)
	, mSpreadLevel(1)
	, mMissileAmmo(2)
	, mDropPickupCommand()
	, mTravelledDistance(0.f)
	, mDirectionIndex(0)
	, mMissileDisplay(nullptr)
	, mIdentifier(0)
	, mDir(North)
	,oldDir(North)
{
	//setDir(North);
	std::map<Aircraft::Direction, std::string> dirMap;
	dirMap[North] = "North";
	dirMap[East] = "East";
	dirMap[South] = "South";
	dirMap[West] = "West";
	mExplosion.setFrameSize(sf::Vector2i(256, 256));
	mExplosion.setNumFrames(16);
	mExplosion.setDuration(sf::seconds(1));

	centerOrigin(mSprite);
	centerOrigin(mExplosion);

	mFireCommand.category = Category::SceneAirLayer;
	mFireCommand.action = [this, &textures, mDir](SceneNode& node, sf::Time)
	{
		createBullets(node, textures);
	};

	mMissileCommand.category = Category::SceneAirLayer;
	mMissileCommand.action = [this, &textures, mDir](SceneNode& node, sf::Time)
	{
		createProjectile(node, Projectile::Missile, 0.f, 0.5f, textures);
	};

	mDropPickupCommand.category = Category::SceneAirLayer;
	mDropPickupCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createPickup(node, textures);
	};

	std::unique_ptr<TextNode> healthDisplay(new TextNode(fonts, ""));
	mHealthDisplay = healthDisplay.get();
	attachChild(std::move(healthDisplay));

	if (getCategory() == Category::PlayerAircraft)
	{
		std::unique_ptr<TextNode> missileDisplay(new TextNode(fonts, ""));
		missileDisplay->setPosition(0, 70);
		mMissileDisplay = missileDisplay.get();
		attachChild(std::move(missileDisplay));
	}

	updateTexts();
}

int Aircraft::getMissileAmmo() const
{
	return mMissileAmmo;
}

void Aircraft::setMissileAmmo(int ammo)
{
	mMissileAmmo = ammo;
}

void Aircraft::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (isDestroyed() && mShowExplosion)
		target.draw(mExplosion, states);
	else
		target.draw(mSprite, states);
}

void Aircraft::disablePickups()
{
	mPickupsEnabled = false;
}

void Aircraft::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	// Update texts and roll animation
	updateTexts();
	updateRollAnimation();
	//checkDirection();

	// Entity has been destroyed: Possibly drop pickup, mark for removal
	if (isDestroyed())
	{
		checkPickupDrop(commands);
		mExplosion.update(dt);

		// Play explosion sound only once
		if (!mExplosionBegan)
		{
			SoundEffect::ID soundEffect = (randomInt(2) == 0) ? SoundEffect::Explosion1 : SoundEffect::Explosion2;
			playLocalSound(commands, soundEffect);

			// Emit network game action for enemy explosions
			if (!isAllied())
			{
				sf::Vector2f position = getWorldPosition();

				Command command;
				command.category = Category::Network;
				command.action = derivedAction<NetworkNode>([position](NetworkNode& node, sf::Time)
				{
					node.notifyGameAction(GameActions::EnemyExplode, position);
				});

				commands.push(command);
			}

			mExplosionBegan = true;
		}
		return;
	}

	// Check if bullets or missiles are fired
	checkProjectileLaunch(dt, commands);

	// Update enemy movement pattern; apply velocity
	updateMovementPattern(dt);
	Entity::updateCurrent(dt, commands);
}

unsigned int Aircraft::getCategory() const
{
	if (isAllied())
		return Category::PlayerAircraft;
	else
		return Category::EnemyAircraft;
}

sf::FloatRect Aircraft::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

bool Aircraft::isMarkedForRemoval() const
{
	return isDestroyed() && (mExplosion.isFinished() || !mShowExplosion);
}

void Aircraft::remove()
{
	Entity::remove();
	mShowExplosion = false;
}

bool Aircraft::isAllied() const
{
	return mType == Eagle;
}

float Aircraft::getMaxSpeed() const
{
	return Table[mType].speed;
}

void Aircraft::increaseFireRate()
{
	if (mFireRateLevel < 10)
		++mFireRateLevel;
}

void Aircraft::increaseSpread()
{
	if (mSpreadLevel < 3)
		++mSpreadLevel;
}

void Aircraft::collectMissiles(unsigned int count)
{
	mMissileAmmo += count;
}

void Aircraft::fire()
{
	// Only ships with fire interval != 0 are able to fire
	if (Table[mType].fireInterval != sf::Time::Zero)
		mIsFiring = true;
}

void Aircraft::launchMissile()
{
	if (mMissileAmmo > 0)
	{
		mIsLaunchingMissile = true;
		--mMissileAmmo;
	}
}

void Aircraft::playLocalSound(CommandQueue& commands, SoundEffect::ID effect)
{
	sf::Vector2f worldPosition = getWorldPosition();

	Command command;
	command.category = Category::SoundEffect;
	command.action = derivedAction<SoundNode>(
		[effect, worldPosition](SoundNode& node, sf::Time)
	{
		node.playSound(effect, worldPosition);
	});

	commands.push(command);
}

int	Aircraft::getIdentifier()
{
	return mIdentifier;
}

void Aircraft::setIdentifier(int identifier)
{
	mIdentifier = identifier;
}
Aircraft::Direction Aircraft::getDir() const
{
	return mDir;
}
void Aircraft::setDir(Direction dir)
{
	mDir = dir;
}
Aircraft::Direction Aircraft::getOldDir() const
{
	return oldDir;
}
void Aircraft::setOldDir(Direction dir)
{
	oldDir = dir;
}
/*First attempt at rotating sprite, resulted in spinning top sprite. Moved to Tom's boolean checks down at bottom LOC 440ish :-)
void Aircraft::rotate()
{
	if (mDir = North)
	if (pointing = North);
	else if (pointing = East)
		mSprite.rotate(90.f);
	else if (pointing = South)
		mSprite.rotate(180.f);
	else if (pointing = West)
		mSprite.rotate(270);
}
*/
void Aircraft::updateMovementPattern(sf::Time dt)
{
	// Enemy airplane: Movement pattern
	/*Editing out as not using this at the moment
	const std::vector<Direction>& directions = Table[mType].directions;
	if (!directions.empty())
	{
		// Moved long enough in current direction: Change direction
		if (mTravelledDistance > directions[mDirectionIndex].distance)
		{
			mDirectionIndex = (mDirectionIndex + 1) % directions.size();
			mTravelledDistance = 0.f;
		}

		// Compute velocity from direction
		float radians = toRadian(directions[mDirectionIndex].angle + 90.f);
		float vx = getMaxSpeed() * std::cos(radians);
		float vy = getMaxSpeed() * std::sin(radians);

		setVelocity(vx, vy);

		mTravelledDistance += getMaxSpeed() * dt.asSeconds();
	}*/
}

void Aircraft::checkPickupDrop(CommandQueue& commands)
{
	if (!isAllied() && randomInt(3) == 0 && !mSpawnedPickup)
		commands.push(mDropPickupCommand);

	mSpawnedPickup = true;
}

void Aircraft::checkProjectileLaunch(sf::Time dt, CommandQueue& commands)
{
	// Enemies try to fire all the time
	if (!isAllied())
		fire();

	// Check for automatic gunfire, allow only in intervals
	if (mIsFiring && mFireCountdown <= sf::Time::Zero)
	{
		// Interval expired: We can fire a new bullet
		commands.push(mFireCommand);
		playLocalSound(commands, isAllied() ? SoundEffect::AlliedGunfire : SoundEffect::EnemyGunfire);

		mFireCountdown += Table[mType].fireInterval / (mFireRateLevel + 1.f);
		mIsFiring = false;
	}
	else if (mFireCountdown > sf::Time::Zero)
	{
		// Interval not expired: Decrease it further
		mFireCountdown -= dt;
		mIsFiring = false;
	}

	// Check for missile launch
	if (mIsLaunchingMissile)
	{
		commands.push(mMissileCommand);
		playLocalSound(commands, SoundEffect::LaunchMissile);

		mIsLaunchingMissile = false;
	}
}

void Aircraft::createBullets(SceneNode& node, const TextureHolder& textures) const
{
	
	Projectile::Type type = isAllied() ? Projectile::AlliedBullet : Projectile::EnemyBullet;
	createProjectile(node, type, 0.0f, 0.0f, textures);
	//Below was attempt to make bullet spawn at correct point on sprite based on rotation...
	/*float current = getRotation();
	if (current == 0)
		createProjectile(node, type, 0.5f, 0.0f, textures);
	else if (current == 270)
		createProjectile(node, type, 0.0f, -0.5f, textures);
	else if (current == 180)
		createProjectile(node, type, 0.5f, 1.0f, textures);
	else if (current == 90)
		createProjectile(node, type, 1.0f, -0.5f, textures);*/

	/*switch (mSpreadLevel) Disabling as we won't use spread, but might work for shotgun in later version
	{
	case 1:
		createProjectile(node, type, 0.0f, 0.5f, textures);
		break;

	case 2:
		createProjectile(node, type, -0.33f, 0.33f, textures);
		createProjectile(node, type, +0.33f, 0.33f, textures);
		break;

	case 3:
		createProjectile(node, type, -0.5f, 0.33f, textures);
		createProjectile(node, type, 0.0f, 0.5f, textures);
		createProjectile(node, type, +0.5f, 0.33f, textures);
		break;
	}*/
}
//pass mdir into here and change velocity based on direction
void Aircraft::createProjectile(SceneNode& node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder& textures) const
{
	/*putting John's code back in - below was attempt to change bullet direction based on first enum Direction,
		then sprite rotation - not working :(
	std::unique_ptr<Projectile> projectile(new Projectile(type, textures));

	sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
	//sf::Vector2f velocity(0, 0);
	float current = getRotation();
	float speed = projectile->getMaxSpeed();
	float negSpeed = (projectile->getMaxSpeed()) * -1.f;
	if (current == 0)
		sf::Vector2f velocity(0, speed);
	else if (current == 270)
		sf::Vector2f velocity(negSpeed, 0);
	else if (current == 180)
		sf::Vector2f velocity(0, negSpeed);
	else if (current == 90)
		sf::Vector2f velocity(speed, 0);
	//velocity = tempVelocity; DON't NEED IT
	float sign = isAllied() ? -1.f : +1.f;
	projectile->setPosition(getWorldPosition() + offset);//removed sign
	projectile->setVelocity(velocity);//removed sign
	node.attachChild(std::move(projectile));*/
	std::unique_ptr<Projectile> projectile(new Projectile(type, textures));

	sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
	sf::Vector2f velocity(0, projectile->getMaxSpeed());

	float sign = isAllied() ? -1.f : +1.f;
	projectile->setPosition(getWorldPosition() + offset * sign);
	projectile->setVelocity(velocity * sign);
	node.attachChild(std::move(projectile));
}

void Aircraft::createPickup(SceneNode& node, const TextureHolder& textures) const
{
	auto type = static_cast<Pickup::Type>(randomInt(Pickup::TypeCount));

	std::unique_ptr<Pickup> pickup(new Pickup(type, textures));
	pickup->setPosition(getWorldPosition());
	pickup->setVelocity(0.f, 1.f);
	node.attachChild(std::move(pickup));
}

void Aircraft::updateTexts()
{
	// Display hitpoints
	if (isDestroyed())
		mHealthDisplay->setString("");
	else
		mHealthDisplay->setString(toString(getHitpoints()) + " HP");
	mHealthDisplay->setPosition(0.f, 50.f);
	mHealthDisplay->setRotation(-getRotation());

	// Display missiles, if available
	if (mMissileDisplay)
	{
		if (mMissileAmmo == 0 || isDestroyed())
			mMissileDisplay->setString("");
		else
			mMissileDisplay->setString("M: " + toString(mMissileAmmo));
	}
}

void Aircraft::updateRollAnimation()
{
	/*if (Table[mType].hasRollAnimation) Disabling as new avatar won't be plane and likely won't be animated for CA2
	{
		sf::IntRect textureRect = Table[mType].textureRect;

		// Roll left: Texture rect offset once
		if (getVelocity().x < 0.f)
			textureRect.left += textureRect.width;

		// Roll right: Texture rect offset twice
		else if (getVelocity().x > 0.f)
			textureRect.left += 2 * textureRect.width;

		mSprite.setTextureRect(textureRect);
	}*/
}
void Aircraft::pointSprite(float degrees)
{
	setOldDir(getDir());
	if (degrees == 0.0f)
		setDir(North);
	else if (degrees == 90.0f)
		setDir(East);
	else if (degrees == 180.0f)
		setDir(South);
	else if (degrees == 270.0f)
		setDir(West);
}
void Aircraft::updatePosition()//method name should be more descripte "updateCollisionPosition" etc.
{
	if (planeIsMovingRight)
	{
		//minus the x
		sf::Vector2f position = getPosition();
		position.x -= 50;//right is plus in the x
		setPosition(position);
		//mSprite.rotate(90);
		/*setOldDir(getDir());
		setDir(East);
		Direction currentDir = getOldDir();
		if (currentDir = North)
			mSprite.setRotation(-90);
		else if (currentDir = South)
			mSprite.setRotation(90);
		else if (currentDir = West)
			mSprite.setRotation(180);*/

	}

	else if (planeIsMovingLeft)
	{
		//plus the x
		sf::Vector2f position = getPosition();
		position.x += 50; 
		setPosition(position);
		//mSprite.rotate(270);
		/*setOldDir(getDir());
		setDir(West);
		Direction currentDir = getOldDir();
		if (currentDir = North)
			mSprite.setRotation(-90);
		else if (currentDir = East)
			mSprite.setRotation(180);
		else if (currentDir = South)
			mSprite.rotate(90);*/

	}

	else if (planeIsMovingDown)
	{
		//minus the y
		sf::Vector2f position = getPosition();
		position.y -= 50;
		setPosition(position);
		//mSprite.rotate(180);
		/*setOldDir(getDir());
		setDir(South);
		Direction currentDir = getOldDir();
		if (currentDir = North)
			mSprite.setRotation(180);
		else if (currentDir = East)
			mSprite.setRotation(-90);
		else if (currentDir = West)
			mSprite.setRotation(90);*/
	}

	else if (planeIsMovingUp)
	{
		//plus the y
		sf::Vector2f position = getPosition();
		position.y += 50;//y decreases towards top thus -
		setPosition(position);
		/*setOldDir(getDir());
		setDir(North);
		Direction currentDir = getOldDir();
		if (currentDir = East)
			mSprite.setRotation(-90);
		else if (currentDir = South)
			mSprite.setRotation(180);
		else if (currentDir = West)
			mSprite.setRotation(90);*/
	}
}