#include "Entity.hpp"

#include <cassert>

Entity::Entity(int hitpoints)
	:mVelocity(), mHitpoints(hitpoints)
{}

void Entity::setVelocity(sf::Vector2f velocity)
{
	mVelocity = velocity;
}

void Entity::setVelocity(float vx, float vy)
{
	mVelocity.x = vx;
	mVelocity.y = vy;
}

sf::Vector2f Entity::getVelocity() const
{
	return mVelocity;
}
//Here's where acceleration happents
void Entity::accelerate(sf::Vector2f velocity)
{
	mVelocity += velocity;
}

void Entity::accelerate(float vx, float vy)
{
	mVelocity.x += vx;
	mVelocity.y += vy;
}

void Entity::updateCurrent(sf::Time dt, CommandQueue&) // Changing to new version per Tom & Now changing back...
{
	move(mVelocity * dt.asSeconds());
}
/*void Entity::updateCurrent(sf::Time dt, CommandQueue&)//TOM's Version - doesn't seem to work right
{
	move(mVelocity * dt.asSeconds());
	mVelocity.x = 0.f;
	mVelocity.y = 0.f;
}*/
int Entity::getHitpoints() const
{
	return mHitpoints;
}

void Entity::setHitpoints(int points)
{
	assert(points > 0);
	mHitpoints = points;
}

void Entity::repair(int points)
{
	assert(points > 0);
	mHitpoints += points;
}

void Entity::damage(int points)
{
	assert(points > 0);
	mHitpoints -= points;
}

void Entity::destroy()
{
	mHitpoints = 0;
}

void Entity::remove()
{
	destroy();
}

bool Entity::isDestroyed() const
{
	return mHitpoints <= 0;
}

void Entity::isWin(bool nuke)
{
	win = nuke;
}
bool Entity::won()
{
	return win;
}
